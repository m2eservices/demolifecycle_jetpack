package com.univlille.lifecycle_demo;


import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.OnLifecycleEvent;

/*
    Classe observateur du cycle de vie de l'activité principale
    Fonctionne grâce à Jetpack
 */

/*
    Au lancement on aura :
Demo_life_cycle Activité: onCreate
Demo_life_cycle Mon_Observateur: onStart
Demo_life_cycle Mon_Observateur: onStop
Demo_life_cycle Mon_Observateur: onCreate depuis ma fonction
Demo_life_cycle Mon_Observateur: il se passe l'événement CREATED
Demo_life_cycle Mon_Observateur: il se passe l'événement STARTED
Demo_life_cycle Activité: onResume
Demo_life_cycle Mon_Observateur: il se passe l'événement RESUMED

    Si on tourne le téléphone (portrait->paysage) :
Demo_life_cycle Mon_Observateur: il se passe l'événement STARTED
Demo_life_cycle Activité: onPause
Demo_life_cycle Mon_Observateur: il se passe l'événement CREATED
Demo_life_cycle Mon_Observateur: il se passe l'événement DESTROYED
Demo_life_cycle Activité: onCreate
Demo_life_cycle Mon_Observateur: onStart
Demo_life_cycle Mon_Observateur: onStop
Demo_life_cycle Mon_Observateur: onCreate depuis ma fonction
Demo_life_cycle Mon_Observateur: il se passe l'événement CREATED
Demo_life_cycle Mon_Observateur: il se passe l'événement STARTED
Demo_life_cycle Activité: onResume
Demo_life_cycle Mon_Observateur: il se passe l'événement RESUMED

    Si on quitte l'appli :
Demo_life_cycle Mon_Observateur: il se passe l'événement STARTED
Demo_life_cycle Activité: onPause
Demo_life_cycle Mon_Observateur: il se passe l'événement CREATED
Demo_life_cycle Mon_Observateur: il se passe l'événement DESTROYED

 */

public class MonObservateur implements LifecycleObserver, LifecycleOwner {

    private LifecycleRegistry lifecycleRegistry;
    private String LOG_TAG = "Demo_life_cycle Mon_Observateur";

    MonObservateur() {
        lifecycleRegistry = new LifecycleRegistry(this);
       // lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return lifecycleRegistry;
    }

    void startOwner() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START);
        Log.i(LOG_TAG, "onStart");
    }

    void stopOwner() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP);
        Log.i(LOG_TAG, "onStop");
    }

    // autre solution pour s'abonner à des événements (ici le OnCreate)
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void maFonction() {
        Log.i(LOG_TAG, "onCreate depuis ma fonction");
    }

    // une solution pour s'abonner à tout type d'événement
    @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    public void onAny(LifecycleOwner owner, Lifecycle.Event event) {
        Log.i(LOG_TAG, "il se passe l'événement " + owner.getLifecycle().getCurrentState().name());
    }

}
