package com.univlille.lifecycle_demo;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

/*
    Petit exemple qui montre comment utiliser jetpack pour une meilleure gestion de l'écoute du cycle de vie
    Source : https://www.techotopia.com/index.php/An_Android_Jetpack_Lifecycle_Tutorial
 */

// Activité principale qui crée un observateur pour son cycle de vie.

public class MainActivity extends AppCompatActivity {

    private String LOG_TAG = "Demo_life_cycle Activité";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(LOG_TAG, "onCreate");

        // on crée un observateur pour le cycle de vie de l'activité
        MonObservateur monObservateur = new MonObservateur();
        // on l'abonne au start et stop de l'activité.
        // On pourrait également faire en sorte que ce soit l'observateur qui décide de ce qu'il veut observer.
        monObservateur.startOwner();
        monObservateur.stopOwner();
        // on l'ajoute en tant qu'observateur
        getLifecycle().addObserver(new MonObservateur());
    }

    public void onResume() {
        super.onResume();
        Log.i(LOG_TAG, "onResume");
    }

    public void onPause() {
        super.onPause();
        Log.i(LOG_TAG, "onPause");
    }
}
